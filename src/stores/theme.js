import {writable} from 'svelte/store';
import {browser} from '$app/environment'

let persistedTheme = browser && localStorage.getItem('theme')
const store = writable(
    persistedTheme ? localStorage.getItem('theme') : 'g90'
);
if (browser) {
    store.subscribe(val => localStorage.setItem('theme', val));
}

export default store;
