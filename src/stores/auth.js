import {writable} from 'svelte/store';
import {browser} from '$app/environment'

let persistedAuth = browser && localStorage.getItem('auth')
const store = writable(
    persistedAuth
        ? (
            localStorage.getItem('auth') !== 'null'
                ? localStorage.getItem('auth')
                : null
        )
        : null
);
if (browser) {
    store.subscribe(val => localStorage.setItem('auth', val));
}

export default store;
