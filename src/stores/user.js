import {writable} from 'svelte/store';
import {browser} from '$app/environment'

let persistedUser = browser && localStorage.getItem('user')
const store = writable(
    persistedUser
        ? (
            localStorage.getItem('user') !== ''
                ? JSON.parse(localStorage.getItem('user'))
                : null
        )
        : null
);
if (browser) {
    store.subscribe(val => localStorage.setItem('user', JSON.stringify(val)));
}

export default store;
