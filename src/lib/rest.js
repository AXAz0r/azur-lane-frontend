class RESTHandler {
    constructor() {
        this.base = 'http://localhost:4080';
        this.api  = `${this.base}/api/v1`;
    }

    async call(method, endpoint, data, token = null) {
        const options = {
            method,
            headers: {
                'Content-Type': 'application/json',
            }
        };

        if (token) {
            options.headers['Authorization'] = `Bearer ${token}`;
        }

        if (data) {
            options.body = JSON.stringify(data);
        }

        const response = await fetch(endpoint, options);
        return await response.json();
    }

    async get(endpoint, token = null) {
        return await this.call('GET', endpoint, null, token);
    }

    async post(endpoint, data, token = null) {
        return await this.call('POST', endpoint, data, token);
    }

    async put(endpoint, data, token = null) {
        return await this.call('PUT', endpoint, data, token);
    }

    async patch(endpoint, data, token = null) {
        return await this.put(endpoint, data, token);
    }

    async delete(endpoint, token) {
        return await this.call('DELETE', endpoint, null, token);
    }

    async root() {
        return await this.get(this.base);
    }

    async login(username, password) {
        return await this.post(`${this.api}/auth/login`, { username, password });
    }

    async self(token) {
        return await this.get(`${this.api}/auth/login`,  token);
    }

    async keys(token) {
        return await this.get(`${this.api}/models/auth/key`, token);
    }

    async createKey(token, name) {
        return await this.post(`${this.api}/models/auth/key`, {name}, token);
    }

    async updateKey(token, key, name) {
        return await this.patch(`${this.api}/models/auth/key/${key}`, {name}, token);
    }

    async deleteKey(token, key) {
        return await this.delete(`${this.api}/models/auth/key/${key}`, token);
    }
}

export default RESTHandler;
